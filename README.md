## Github Users

Pequeno projeto para testar o uso do Ecmascript 2017 com knockoutjs.

O projeto faz a busca do usuário algumas de suas informações no Github.


### Run

Para rodar, basta clonar o repositório e dar os seguintes comandos:

    npm install

    npm run dev
