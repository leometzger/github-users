import "babel-polyfill";
import GithubService from './fetcher/GithubService';
import User from './models/User';

class IndexVM {

    constructor(){
        this.api        = new GithubService();
        this.users      = ko.observableArray([]);
        this.username   = ko.observable("");
        this.user       = ko.observable(null);
    }

    async getUser(){
        const username = this.username();
        const user = await this.api.getUser(username);
        this.user(user);

        if(user){
            this.users.push(user);
            this.username("");
        }
    }   
}

ko.applyBindings(new IndexVM());