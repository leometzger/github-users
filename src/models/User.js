export default class User {

    constructor(name = "", city = "", company = "", image = "", 
                followers = 0, following = 0, url = ""){
        this.name       = name;
        this.city       = city;
        this.company    = company;
        this.image      = image;
        this.following  = following;
        this.followers  = followers;
        this.htmlURL    = url;
    }

    toString(){
        return `Name: ${this.name} - City: ${this.city} - Company: ${this.company}`;
    }
}