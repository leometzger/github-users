import axios from 'axios';
import User from '../models/User';

export default class GithubService {

    async getUser(username){
        try {
            const url = `https://api.github.com/users/${username}`;
            const response = await axios.get(url);
            const data = response.data;

            return new User(data.name,      data.location,  data.company,
                            data.avatar_url, data.followers, data.following);
        }
        catch(err){
            console.log(err);
            return null;
        }
    }

}